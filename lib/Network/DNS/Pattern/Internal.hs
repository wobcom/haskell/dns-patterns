-- |
-- Module      : Network.DNS.Pattern.Internal
-- Description : Internal pattern types and definitions
--
-- This module is not part of public API and may change even between patch versions.
module Network.DNS.Pattern.Internal
  ( DomainLabel(..)
  , Domain(..)
  , LabelPattern(..)
  , DomainPattern(..)
  )

where

import Network.DNS.Internal
-- | A domain pattern.
newtype DomainPattern = DomainPattern
  { getDomainPattern :: [LabelPattern]
  } deriving (Eq, Ord)

-- | A pattern for a singular label.
data LabelPattern
  = DomLiteral DomainLabel -- ^ Represents an exact label that must be matched.
  | DomGlob -- ^ Represents a single asterisk glob matching any arbitrary domain at a given level.
  | DomGlobStar -- ^ Represents a double asterisk matching any arbitrary subdomain at a given level.
  deriving (Eq, Ord)
